#!/usr/bin/env bash

# Get the second to last commit from the main branch.
#
PREVIOUS_COMMIT=`git rev-parse master~`

# echo $PREVIOUS_COMMIT - debug
# echo $BITBUCKET_COMMIT - debug

# Get the names of the modified PHP files in the second to last and last commits.
#
STAGED_FILES=`git diff --name-only  $PREVIOUS_COMMIT...$BITBUCKET_COMMIT | grep \\\\.php`

for FILE in $STAGED_FILES
do
	# echo $FILE - uncomment to see what files are being lint
	php -l -d display_errors=0 $FILE
	if [ $? != 0 ]
	then
		exit 1 # Fail the build
	fi
done

exit 0 # Successful build