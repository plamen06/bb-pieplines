<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TodoList extends Model
{
    /**
     * The model's default values for attributes.
     *asdasdd
     * @var array
     */
    protected $attributses = [
        'completed' => false,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title'];

    /**
     * Get the items of a list
     */
    public function items()
    {
        return $this->hasMany('App\Models\Item');
    }
}
